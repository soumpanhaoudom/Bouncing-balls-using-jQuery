		// get window screen witdth and height	
			var windowWidth = window.innerWidth;
			var windowHeight = window.innerHeight
	

			// ball1
			function ball1(){
				$('#ball1').animate({left:windowWidth-150,top:windowHeight-150},600);
				$('#ball1').animate({left:'0px'},600);
				$('#ball1').animate({top:'0px'},600);
			}
			
			// ball2
			function ball2(){
				$('#ball2').animate({right:200,bottom:0},300);
				$('#ball2').animate({right:400,bottom:windowHeight-100},300);
				$('#ball2').animate({right:600,bottom:0},300);
				$('#ball2').animate({right:800,bottom:windowHeight-100},300);
				$('#ball2').animate({right:windowWidth-100,bottom:0},300);
				$('#ball2').animate({right:0,bottom:windowHeight-100},300);
			}

			// ball3
			function ball3(){
				$('#ball3').animate({right:windowWidth-150,bottom:windowHeight-150},1000);
				$('#ball3').animate({right:'0px'},1000);
				$('#ball3').animate({bottom:'0px'},1000);
			}

			// ball4
			function ball4(){
				$('#ball4').animate({left:windowWidth-100,top:(windowHeight-100)/2},800);
				$('#ball4').animate({left:"50%", top:windowHeight-100},800);
				$('#ball4').animate({left:0, top:(windowHeight-100)/2},800);
				$('#ball4').animate({left:"50%", top:0},800);
			}

			// ball5
			function ball5(){
				$('#ball5').animate({right:200,bottom:0},500);
				$('#ball5').animate({right:windowWidth-100,bottom:200},500);
				$('#ball5').animate({right:windowWidth-300,bottom:windowHeight-100},500);
				$('#ball5').animate({right:0,bottom:200},500);
			}

			// ball6
			function ball6(){
				$('#ball6').animate({left:300,bottom:0},2300);
				$('#ball6').animate({left:windowWidth-300,bottom:windowHeight-200},2300);
				$('#ball6').animate({left:windowWidth-200,bottom:windowHeight-300},2300);
				$('#ball6').animate({left:300,bottom:0},2300);
				$('#ball6').animate({left:00,bottom:windowHeight-300},2300);
				$('#ball6').animate({left:0,bottom:windowHeight-200},2300);
				$('#ball6').animate({left:300,bottom:0},2300);
			}

			// ball7
			function ball7(){
				$('#ball7').animate({left:windowWidth-100,bottom:(windowHeight-100)/2},800);
				$('#ball7').animate({left:"50%", bottom:windowHeight-100},800);
				$('#ball7').animate({left:0, bottom:(windowHeight-100)/2},800);
				$('#ball7').animate({left:"50%", bottom:0},800);
			}

			// ball8
			function ball8(){
				$('#ball8').animate({left:200,bottom:0},300);
				$('#ball8').animate({left:400,bottom:windowHeight-100},300);
				$('#ball8').animate({left:600,bottom:0},300);
				$('#ball8').animate({left:800,bottom:windowHeight-100},300);
				$('#ball8').animate({left:windowWidth-100,bottom:0},300);
				$('#ball8').animate({left:0,bottom:windowHeight-100},300);
			}

			// call all function above to execute forever by setInterval()
			$(document).ready(function(){
				setInterval(function(){
					ball1();
					ball2();
					ball3();
					ball4();
					ball5();
					ball6();
					ball7();
					ball8();
				});
			});